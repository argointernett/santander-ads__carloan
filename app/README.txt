-- REQUIRED GET PARAMETER FOR THE LEFT sidebar- 
- left_sidebar: must be 'true' if on the left sidebar ad.


-- 'GET' PARAMETERS SENT ALONG WITH 'form_submit_url' AFTER THE 'CTA' BUTTON IS PRESSED: --
- price: product price;
- equity: starting capital;
- repayment: payback period, in years.


-- OPTIONAL 'GET' PARAMETERS FOR THE INDEX.HTML FILE (all must be URL-encoded): --

PARAMETERS USED WITH THE HORSESHOE ADS
- bg_image_180: the URL of the image to be displayed at the background of the left sidebar ad;
- small_slide_title: title to be displayed on the left sidebar ad;
- small_slide_subtitle: subtitle to be displayed on the left sidebar ad;
- top_bg_image: optional image to be displayed at the background of the top sidebar ad;
- top_title: small uppercase title on the top sidebar ad;
- top_header: large lowercase text on the top sidebar ad;
- top_disclaimer: small disclaimer text on the top sidebar ad.

PARAMETERS USED ON THE 320PX VERSION
- large_slide_title: small uppercase title to be displayed on the first slide;
- large_slide_header: larger lowercase text to be displayed on the first slide;
- large_slide_text: small text to be displayed on the first slide.
- bg_320_image: the URL of the background image;

PARAMETERS USED WITH THE INSTALLMENT CALCULATOR
- button_text: label of the CTA button;
- item_price: the starting price of the product. Default is '200.000';
- capital: the starting amount of capital inputted but the user. Default is '100.000';
- payback_period: the starting amount of years. Default is '5';
- product_image: the URL of the product image;
- price_title: title of product price;
- effective_interest: value displayed next to 'Eff. rente'. This is used for calculating installments;
- establishment_fee: used to calculate monthly installments. Default is '2400';
- registration_fee: used to calculate monthly installments. Default is '1051';
- installment_fee: used to calculate monthly installments. Default is '75';
- disclaimer_text: small text to be displayed at the bottom of the second slide on the 320px version.